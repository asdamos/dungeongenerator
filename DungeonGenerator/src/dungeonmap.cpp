#include "dungeonmap.h"
#include <cstdlib>
#include <algorithm>
#include <climits>
#include <iostream>

#include <QDebug>
#include <assert.h>
#include <ctime>
#include <queue>

#include "floorattachment.h"

DungeonMap::DungeonMap()
{
    srand(time(nullptr));
}


static std::vector<int> shuffledValues(int from, int to){
    assert( from <= to );

    std::vector<int> values;
    values.reserve(to-from+1);

    for (int i = from; i <= to; i++)
    {
        values.push_back(i);
    }

    std::random_shuffle(values.begin(), values.end());
    return values;
}


bool DungeonMap::collideWithAnything(Room &newRoom)
{
    bool result = false;
    for (Room *r : rooms)
    {
        result |= newRoom.isColliding(r);
    }
    return result;
}


void DungeonMap::roomInRandomPlace(int rWidth, int rHeight)
{
    Room *newRoom;
    std::vector<int> randRoomIndices = shuffledValues(0, rooms.size()-1);

    for (int i : randRoomIndices)
    {
        Room *randRoom = rooms[i];

        std::vector<int> randDirections = shuffledValues(0, 3);

        for (int k : randDirections)
        {
            newRoom = nullptr;            
            wallsDirections randWall = wallsDirections(randDirections[k]);

            for (int j : possiblePositionsOnWall(randRoom, randWall))
            {
                newRoom = attachToRoom(randRoom, rWidth, rHeight, randWall, j);

                if (!collideWithAnything(*newRoom))
                {
                    rooms.push_back(newRoom);
                    connections.push_back(std::make_pair(randRoom, newRoom));
                    return;
                }
                else
                {
                    delete newRoom;
                }
            }
        }
    }
    assert(false);
}


std::vector<int> DungeonMap::possiblePositionsOnWall(Room *base, wallsDirections wall)
{
    std::vector<int> randWallField;

    if (wall == wallsDirections::LEFT || wall == wallsDirections::RIGHT)
    {
        randWallField = shuffledValues(base->getY(), base->getYend() - 2);
    }
    else
    {
        randWallField = shuffledValues(base->getX(), base->getXend() - 2);
    }
    return randWallField;
}


Room* DungeonMap::attachToRoom(Room *base, int newRoomWidth, int newRoomHeight,
                               wallsDirections wall, int posOnWall)
{
    Room* newRoom = nullptr;
    switch(wall)
    {
        case wallsDirections::RIGHT:
            newRoom = new Room(base->getXend(), posOnWall, newRoomWidth, newRoomHeight);
            break;
        case wallsDirections::LEFT:
            newRoom = new Room(base->getX() - newRoomWidth + 1, posOnWall, newRoomWidth, newRoomHeight);
            break;
        case wallsDirections::UP:
            newRoom = new Room(posOnWall, base->getYend(), newRoomWidth, newRoomHeight);
            break;
        case wallsDirections::DOWN:
            newRoom = new Room(posOnWall, base->getY() - newRoomHeight + 1, newRoomWidth, newRoomHeight);
    }
    return newRoom;
}



void DungeonMap::nextRoom()
{
    if(config.getRoomsNumber() == 0)
    {
        assert(false);
        return;
    }
    int rHeight = config.getMinHeight() +
                 (std::rand() % (config.getMaxHeight() - config.getMinHeight() + 1) );
    int rWidth  = config.getMinWidth() +
                 (std::rand() % (config.getMaxWidth() - config.getMinWidth() + 1) );

    if (rooms.empty())
    {
        rooms.push_back(new Room(0, 0, rWidth, rHeight));

    }
    else
    {
        roomInRandomPlace(rWidth, rHeight);
    }
}

void DungeonMap::generate(GeneratorAttributes config)
{
    this->config = config;
    rooms.reserve(config.getRoomsNumber());
    for (unsigned int i = 0; i < config.getRoomsNumber(); i++)
    {
        nextRoom();
    }
    generateRoomsDoors();

    if(config.getGenerateAttachments())
    {
        //generateRandomAttachments();
        simpleGeneratorOfRandomAttachments();
    }
}

void DungeonMap::writeToXml(QXmlStreamWriter &xmlStream)
{
    QString number;
    number.setNum(rooms.size());

    xmlStream.writeStartElement("map");
    xmlStream.writeAttribute("rooms", number);

    for(Room *r : rooms)
    {
        
        r->writeToXml(xmlStream);
    }

    xmlStream.writeEndElement();
}

void DungeonMap::parseXML (QXmlStreamReader &xmlStream)
{
    QXmlStreamAttributes atr = xmlStream.attributes();
    if( atr.hasAttribute( "rooms" ) )
    {
        int roomsNo = atr.value("rooms").toInt();

        if( roomsNo < 1 )
        {
            xmlStream.raiseError("Rooms number expected to be greater than 0. Got " + QString::number(roomsNo) + " instead.");
            return;
        }

        for(int i=0; i<roomsNo; ++i)
        {
            QXmlStreamReader::TokenType token = xmlStream.readNext(); // we have to do readNext() twice because first one is characters between tags. In our case is literally nothing ...
            token = xmlStream.readNext();

            //qDebug() << "Next room!";

            if(token != QXmlStreamReader::StartElement) xmlStream.raiseError("Wrong type of token - " + xmlStream.tokenString() + ". Expected: StartElement");
            if(xmlStream.name() != "room")
            {
                xmlStream.raiseError("Wrong token - \"" + xmlStream.name().toString() + "\". Expected: room");
                return;
            }

            atr = xmlStream.attributes();

            if( ! atr.hasAttribute("x") )
            {
                xmlStream.raiseError("Attribute \"x\" not found.");
                return;
            }

            int x = atr.value("x").toInt();
            int y = atr.value("y").toInt();
            int w = atr.value("width").toInt();
            int h = atr.value("height").toInt();

            Room* room = new Room (x,y,w,h,false);

            if( ! room->parseXML(xmlStream) ){
                return;
            }

            this->rooms.push_back(room);


            token = xmlStream.readNext();
            token = xmlStream.readNext();
        }
    }
    else
    {
        // we can handle that ! but not now
        xmlStream.raiseError("Attribute \"rooms\" not found.");
    }
}


void DungeonMap::cleanUp()
{
    for (unsigned int i = 0; i < rooms.size(); i++)
    {
        delete rooms[i];
    }
    rooms.clear();
    connections.clear();
}


bool DungeonMap::isEmpty()
{
    return rooms.empty();
}

std::vector<QVector2D> DungeonMap::getWallsTriangles()
{
    std::vector<QVector2D> triangles;

    for(auto r : rooms)
    {
        float x = r->getX() * 1.0f;
        float y = r->getY() * 1.0;
        float h = r->getHeight() * 1.0;
        float w = r->getWidth() * 1.0;

        triangles.push_back( QVector2D(x,   y)   );
        triangles.push_back( QVector2D(x+w, y)   );
        triangles.push_back( QVector2D(x,   y+h) );
        triangles.push_back( QVector2D(x+w, y+h) );
        triangles.push_back( QVector2D(x,   y+h) );
        triangles.push_back( QVector2D(x+w, y)   );
    }

    return triangles;
}

std::vector<QVector2D> DungeonMap::getRoomsTriangles()
{
    std::vector<QVector2D> triangles;

    for(auto r : rooms)
    {
        float x = r->getX() * 1.0f + 1.0f;
        float y = r->getY() * 1.0 + 1.0f;
        float h = r->getHeight() * 1.0 - 2.0f;
        float w = r->getWidth() * 1.0 - 2.0f;

        triangles.push_back( QVector2D(x,   y)   );
        triangles.push_back( QVector2D(x+w, y)   );
        triangles.push_back( QVector2D(x,   y+h) );
        triangles.push_back( QVector2D(x+w, y+h) );
        triangles.push_back( QVector2D(x,   y+h) );
        triangles.push_back( QVector2D(x+w, y)   );
    }

    return triangles;
}

std::vector<QVector2D> DungeonMap::getDoorTriangles()
{
    std::vector<QVector2D> triangles;

    for(auto r : rooms)
    {
        int h = r->getHeight();
        int w = r->getWidth();

        float roomX = r->getX() * 1.0f;
        float roomY = r->getY() * 1.0f;

        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < w; ++x)
            {
                Field *f = r->fields[x][y];

                assert( f != nullptr );

                if( f->getType() == fieldType::DOOR )
                {

                    triangles.push_back( QVector2D(roomX+x,       roomY+y)      );
                    triangles.push_back( QVector2D(roomX+x+1.0f,  roomY+y)      );
                    triangles.push_back( QVector2D(roomX+x,       roomY+y+1.0f) );
                    triangles.push_back( QVector2D(roomX+x+1.0f,  roomY+y+1.0f) );
                    triangles.push_back( QVector2D(roomX+x,       roomY+y+1.0f) );
                    triangles.push_back( QVector2D(roomX+x+1.0f,  roomY+y)      );

                }
            }
        }
    }

    return triangles;
}

std::vector<QVector2D> DungeonMap::getAttachmentTriangles(attachmentType type)
{
    std::vector<QVector2D> triangles;

    for(auto r : rooms)
    {
        int h = r->getHeight();
        int w = r->getWidth();

        float roomX = r->getX() * 1.0f;
        float roomY = r->getY() * 1.0f;

        for (int y = 0; y < h; ++y)
        {
            for (int x = 0; x < w; ++x)
            {
                Field *f = r->fields[x][y];
                assert( f != nullptr );

                if(f->getType() == fieldType::FLOOR)
                {
                    Floor* floor = dynamic_cast<Floor*> (f);

                    if( floor->getAttachment() != nullptr )
                    {
                        if( floor->getAttachment()->getType() == type )
                        {
                            triangles.push_back( QVector2D(roomX+x+0.2f,    roomY+y+0.2f) );
                            triangles.push_back( QVector2D(roomX+x+0.8f,    roomY+y+0.2f) );
                            triangles.push_back( QVector2D(roomX+x+0.2f,    roomY+y+0.8f) );
                            triangles.push_back( QVector2D(roomX+x+0.8f,    roomY+y+0.8f) );
                            triangles.push_back( QVector2D(roomX+x+0.2f,    roomY+y+0.8f) );
                            triangles.push_back( QVector2D(roomX+x+0.8f,    roomY+y+0.2f) );
                        }
                    }
                }
            }
        }
    }

    return triangles;
}


void DungeonMap::generateOneDoor(Room *r1, Room *r2)
{
    std::vector<std::pair<Field*, Field*> > commonFields;

    int minX = std::min(r1->getX(), r2->getX());
    int maxX = std::max(r1->getXend(), r2->getXend());
    int minY = std::min(r1->getY(), r2->getY());
    int maxY = std::max(r1->getYend(), r2->getYend());

    for (int i = minX; i <= maxX; i++)
    {
        for (int j = minY; j <= maxY; j++)
        {
            if ( r1->containsField(i, j) && r2->containsField(i,j) &&
               !(r1->hasCornerAt(i, j) || r2->hasCornerAt(i, j)) )
            {
                commonFields.push_back(std::make_pair(r1->getField(i,j), r2->getField(i,j)));
            }
        }
    }

    assert(commonFields.size() > 0);

    int randIndex = rand() % commonFields.size();

    *commonFields[randIndex].first = Door();
    *commonFields[randIndex].second = Door();
}



void DungeonMap::generateRoomsDoors()
{
    for (auto conn : connections)
    {
        generateOneDoor(conn.first, conn.second);
    }
}



std::vector<std::string> DungeonMap::mapString()
{
    if (rooms.empty())
    {
        return std::vector<std::string>();
    }

    int minX, maxX, minY, maxY, dX, dY;
    determineMapSize(minX, maxX, minY, maxY, dX, dY);

    std::vector<std::string> output = initOutputMapString(dY, dX);

    for (Room *r : rooms)
    {
        for (int x = minX; x <= maxX; x++)
        {
            unsigned int outputX = x - minX;

            for (int y = minY; y <= maxY; y++)
            {
                unsigned int outputY = y - minY;

                if (r->containsField(x,y))
                {
                    switch (r->getField(x,y)->getType())
                    {
                    case fieldType::WALL:
                        output[outputY][outputX] = '#';
                        break;
                    case fieldType::DOOR:
                        output[outputY][outputX] = 'D';
                        break;
                    case fieldType::FLOOR:
                    {
                        Floor* floor = dynamic_cast<Floor*> (r->getField(x,y));
                        if( floor->getAttachment() == nullptr )
                        {
                            output[outputY][outputX] = '.';
                        }
                        else
                        {
                            switch (floor->getAttachment()->getType())
                            {
                            case TREASURE:
                                output[outputY][outputX] = '$';
                                break;
                            case MONSTER:
                                output[outputY][outputX] = 'M';
                                break;
                            case TRAP:
                                output[outputY][outputX] = 'T';
                                break;
                            default:
                                assert(false &&  "Did you forget about something?");
                                break;
                            }
                        }
                    }
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }
    return output;
}


void DungeonMap::determineMapSize(int &minX, int &maxX, int &minY, int &maxY, int &dX, int &dY)
{
    minX = INT_MAX;
    minY = INT_MAX;
    maxX = INT_MIN;
    maxY = INT_MIN;

    for (Room *r : rooms)
    {
        minX = minX < r->getX()? minX : r->getX();
        minY = minY < r->getY()? minY : r->getY();
        maxX = maxX > r->getXend()? maxX : r->getXend();
        maxY = maxY > r->getYend()? maxY : r->getYend();
    }

    dX = maxX - minX + 1;
    dY = maxY - minY + 1;
}


std::vector<std::string> DungeonMap::initOutputMapString(int xSize, int ySize)
{
    std::vector<std::string> output;
    for (int i = 0; i < xSize; i++)
    {
       std::string str;
       str.resize(ySize);
       for (int j = 0; j < ySize; j++)
       {
           str[j] = ' ';
       }
       output.push_back(str);
    }
    return output;
}


void DungeonMap::console_write()
{
    std:: cout << "Begin:\n";
    for (auto s : mapString())
    {
        std::cout << s << "\n";
    }
    std::cout << "End\n";
}



Room* DungeonMap::getLowestRoom()
{
    if (rooms.size() == 0)
    {
        return nullptr;
    }

    int lowestY = 0;
    Room* lowestRoom = rooms[0];
    for (Room* r : rooms)
    {
        if (lowestY > r->getY())
        {
            lowestY = r->getY();
            lowestRoom = r;
        }
    }
    return lowestRoom;
}


std::map<Room*, int>  DungeonMap::roomsIndices()
{
    std::map<Room*, int> ri;

    for (unsigned int i = 0; i < rooms.size(); i++)
    {
        ri[rooms[i]] = i;
    }

    return ri;
}


std::vector<std::vector<Room*> > DungeonMap::constructConnectionsGraph(
        std::map<Room*, int> &_roomsIndices)
{
    std::vector<std::vector<Room *> > edges(rooms.size(), std::vector<Room*>());
    for (std::pair<Room*, Room*> con : connections)
    {
        int i = _roomsIndices[con.first];
        int j = _roomsIndices[con.second];

        edges[i].push_back(con.second);
        edges[j].push_back(con.first);
    }

    return edges;
}


int DungeonMap::randomValueFromOpenedInterval(int left, int right)
{
    return left + 1 + (rand() % (right - left - 1) );
}


void DungeonMap::addSingleRoomAttachment(Room *r, int attachmentValue)
{
    int x = randomValueFromOpenedInterval(r->getX(), r->getXend());
    int y = randomValueFromOpenedInterval(r->getY(), r->getYend());

    attachmentType randomAttachment = attachmentType(rand() % 3);
    int maxDifference = attachmentValue / 5 + 1;
    attachmentValue += ( (rand() % 3) - 1) * (rand() % maxDifference);

    FloorAttachment *fa;
    switch (randomAttachment)
    {
    case attachmentType::TREASURE:
        fa = new Treasure();
        break;
    case attachmentType::MONSTER:
        fa = new Monster();
        break;
    case attachmentType::TRAP:
        fa = new Trap();
        break;
    default:
        assert(false);
    }

    fa->setValue(attachmentValue);

    Floor* choosenFloor = dynamic_cast<Floor*> (r->getField(x,y));
    choosenFloor->setAttachment(fa);
}


void DungeonMap::generateRandomAttachments()
{
    Room* startingRoom = getLowestRoom();
    if (startingRoom == nullptr)
    {
        return;
    }

    std::map<Room*, int> _roomsIndices = roomsIndices();

    std::vector<std::vector<Room *> > edges = constructConnectionsGraph(_roomsIndices);
    std::vector<bool> visited(false, rooms.size());
    std::queue<Room*> roomsToVisit;

    roomsToVisit.push(startingRoom);

    int roomValue = 10;
    while (!roomsToVisit.empty())
    {

        Room* currentRoom = roomsToVisit.front();
        int currentRoomIndex = _roomsIndices[currentRoom];
        visited[currentRoomIndex] = true;
        addSingleRoomAttachment(currentRoom, roomValue);

        roomsToVisit.pop();

        for (Room* adjacentRoom : edges[currentRoomIndex])
        {
            if (!visited[_roomsIndices[adjacentRoom]])
            {
                roomsToVisit.push(adjacentRoom);
            }
        }

        roomValue += 2;
    }

}


void DungeonMap::simpleGeneratorOfRandomAttachments()
{
    Room* lowestRoom = getLowestRoom();
    for(auto r : rooms)
    {
        int value = r->distance(lowestRoom) + 10;
        value += ((rand() % 3) - 2) * (value / 4);
        int at = rand() % (r->getField()/8 + 1);
        for (int i = 0; i < at; ++i) {
            addSingleRoomAttachment(r, value++);
        }
    }
}
