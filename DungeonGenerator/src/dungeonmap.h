#ifndef DUNGEONMAP_H
#define DUNGEONMAP_H

#include "room.h"
#include "generatorattributes.h"
#include <map>
#include <vector>
#include <iostream>
#include <QXmlStreamWriter>
#include <QVector2D>

class dungeonMapClassTest;

enum wallsDirections {
    LEFT,
    RIGHT,
    UP,
    DOWN
};

class DungeonMap
{
    friend class dungeonMapClassTest;

protected:
    std::vector<Room*>   rooms;
    GeneratorAttributes  config;

    std::vector<std::pair<Room*, Room*>> connections;

public:
    DungeonMap();

    void nextRoom();

    void generate(GeneratorAttributes  config);
    void writeToXml(QXmlStreamWriter & xmlStream);
    void parseXML(QXmlStreamReader & xmlStream);

    std::vector<std::string> mapString();
    void console_write();

    void cleanUp();
    bool isEmpty();

    std::vector<QVector2D> getWallsTriangles();
    std::vector<QVector2D> getRoomsTriangles();
    std::vector<QVector2D> getDoorTriangles();
    std::vector<QVector2D> getAttachmentTriangles(attachmentType type);

protected:
    // checks if newRoom collide with any room from rooms vector
    bool collideWithAnything(Room &newRoom);
    void roomInRandomPlace(int rWidth, int rHeight);
    std::vector<int> possiblePositionsOnWall(Room* base, wallsDirections wall);
    Room* attachToRoom(Room *base, int newRoomWidth, int newRoomHeight, wallsDirections wall,
                       int posOnWall);
    void generateOneDoor(Room* r1, Room *r2);
    void generateRoomsDoors();
    void determineMapSize(int &minX, int &maxX, int &minY, int &maxY, int &dX, int &dY);
    std::vector<std::string> initOutputMapString(int xSize, int ySize);
    Room* getLowestRoom();
    std::vector<std::vector<Room*> > constructConnectionsGraph(
            std::map<Room *, int> &_roomsIndices);
    std::map<Room*, int>  roomsIndices();
    int randomValueFromOpenedInterval(int left, int right);
    void addSingleRoomAttachment(Room *r, int attachmentValue);
    void generateRandomAttachments();

    void simpleGeneratorOfRandomAttachments();
};


#endif // DUNGEONMAP_H
