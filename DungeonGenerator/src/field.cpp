#include "field.h"
#include <assert.h>
#include <QString>
Field::Field (fieldType _type)
    : type(_type)
{

}

void Field::writeToXml(QXmlStreamWriter &xmlStream)
{
    QString typeName;
    switch(type)
    {
    case NONE:
        assert(false && "This shouldnt be NONE"); //
        break;
    case WALL:
        typeName = "wall";
        break;
    case DOOR:
        typeName = "door";
        break;
    case FLOOR:
        typeName = "floor";
        break;
     default:
        assert(false &&  "Did you forget about something?");
        break;
    }

    xmlStream.writeTextElement("type", typeName);

    if( type == FLOOR )
    {
        Floor* floor = dynamic_cast<Floor*> (this);
        if(floor->getAttachment() != nullptr)
        {
            floor->getAttachment()->writeToXml(xmlStream);
        }
        else
        {

            xmlStream.writeTextElement("attachment", "nothing");
        }
    }
}


Wall::Wall()
    : Field(fieldType::WALL)
{

}


Door::Door()
    : Field(fieldType::DOOR)
{

}



Floor::Floor()
    : Field(fieldType::FLOOR)
    , attachment(nullptr)
{

}


Floor::~Floor()
{
    if (attachment != nullptr)
    {
        delete attachment;
    }
}


FloorAttachment* Floor::getAttachment()
{
    return attachment;
}


void Floor::setAttachment(FloorAttachment *_attachment)
{
    attachment = _attachment;
}

