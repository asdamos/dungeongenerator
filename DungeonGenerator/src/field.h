#ifndef FIELD_H
#define FIELD_H
#include <QXmlStreamWriter>
#include "floorattachment.h"

enum fieldType
{
    NONE,
    WALL,
    DOOR,
    FLOOR
};

class Field
{
    fieldType     type;
protected:
    Field(fieldType _field);
public:
    virtual ~Field() { }
    fieldType getType() { return type; }

    void writeToXml(QXmlStreamWriter &xmlStream);
};

// different field
class Wall : public Field
{
public:
    Wall();
};

class Door : public Field
{
public:
    Door();
};

class FloorAttachment;

class Floor : public Field
{
    FloorAttachment *attachment;
public:
    Floor();
    ~Floor();

    FloorAttachment* getAttachment();
    void setAttachment(FloorAttachment *_attachment);
};

#endif // FIELD_H
