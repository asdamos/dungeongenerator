#include "floorattachment.h"
#include "assert.h"

FloorAttachment::FloorAttachment(attachmentType _type)
    : type(_type)
{

}

attachmentType FloorAttachment::getType()
{
    return type;
}

int FloorAttachment::getValue()
{
    return value;
}


void FloorAttachment::setValue(int _value)
{
    value = _value;
}

void FloorAttachment::writeToXml(QXmlStreamWriter &xmlStream)
{
    QString typeName;
    switch (type)
    {
    case TREASURE:
        typeName = "treasure";
        break;
    case MONSTER:
        typeName = "monster";
        break;
    case TRAP:
        typeName = "trap";
        break;
    default:
        assert(false &&  "Did you forget about something?");
    }


    QString number;
    number.setNum(getValue());

    xmlStream.writeStartElement("attachment");
    xmlStream.writeAttribute("value", number);
    xmlStream.writeCharacters(typeName);
    xmlStream.writeEndElement();


}



Treasure::Treasure()
    : FloorAttachment(attachmentType::TREASURE)
{

}


Monster::Monster()
    : FloorAttachment(attachmentType::MONSTER)
{

}


Trap::Trap()
    : FloorAttachment(attachmentType::TRAP)
{

}
