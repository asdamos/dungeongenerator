#ifndef FLOORATTACHMENT_H
#define FLOORATTACHMENT_H

#include <QXmlStreamWriter>

enum attachmentType
{
    TREASURE,
    MONSTER,
    TRAP
};

class FloorAttachment
{
    int value;
    attachmentType type;
protected:
    FloorAttachment(attachmentType _type);
public:
    attachmentType getType();
    int getValue();
    void setValue(int _value);
    void writeToXml(QXmlStreamWriter &xmlStream);
};


class Treasure : public FloorAttachment
{
public:
    Treasure();
};


class Monster : public FloorAttachment
{
public:
    Monster();
};


class Trap : public FloorAttachment
{
public:
    Trap();
};

#endif // FLOORATTACHMENT_H
