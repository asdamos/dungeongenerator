#include "generatorattributes.h"

GeneratorAttributes::GeneratorAttributes()
    : minHeight(0)
    , minWidth(0)
    , maxHeight(0)
    , maxWidth(0)
    , roomsNumber(0)
{   }

unsigned int GeneratorAttributes::getMinWidth() const
{
    return minWidth;
}

void GeneratorAttributes::setMinWidth(unsigned int value)
{
    minWidth = value;
}

unsigned int GeneratorAttributes::getMaxWidth() const
{
    return maxWidth;
}

void GeneratorAttributes::setMaxWidth(unsigned int value)
{
    maxWidth = value;
}

unsigned int GeneratorAttributes::getMinHeight() const
{
    return minHeight;
}

void GeneratorAttributes::setMinHeight(unsigned int value)
{
    minHeight = value;
}

unsigned int GeneratorAttributes::getMaxHeight() const
{
    return maxHeight;
}

void GeneratorAttributes::setMaxHeight(unsigned int value)
{
    maxHeight = value;
}

unsigned int GeneratorAttributes::getRoomsNumber() const
{
    return roomsNumber;
}

void GeneratorAttributes::setRoomsNumber(unsigned int value)
{
    roomsNumber = value;
}

bool GeneratorAttributes::getGenerateAttachments() const
{
    return generateAttachments;
}

void GeneratorAttributes::setGenerateAttachments(bool value)
{
    generateAttachments = value;
}
