#ifndef GENERATORATTRIBUTES_H
#define GENERATORATTRIBUTES_H

//#include <initializer_list>

class GeneratorAttributes
{
    unsigned int  minHeight;
    unsigned int  minWidth;
    unsigned int  maxHeight;
    unsigned int  maxWidth;
    unsigned int  roomsNumber;
    bool          generateAttachments;
public:
    GeneratorAttributes();
    //GeneratorAttributes(std::initializer_list<int> list);

    unsigned int getMinWidth() const;
    void setMinWidth(unsigned int value);

    unsigned int getMaxWidth() const;
    void setMaxWidth(unsigned int value);

    unsigned int getMinHeight() const;
    void setMinHeight(unsigned int value);

    unsigned int getMaxHeight() const;
    void setMaxHeight(unsigned int value);

    unsigned int getRoomsNumber() const;
    void setRoomsNumber(unsigned int value);

    bool getGenerateAttachments() const;
    void setGenerateAttachments(bool value);
};

#endif // GENERATORATTRIBUTES_H
