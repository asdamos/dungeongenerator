#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include "generatorattributes.h"
#include <QDebug>
#include <QFileDialog>
#include <QXmlStreamWriter>
#include <vector>
#include "widget.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->mapDebug->setFontPointSize(9);    

    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setVersion(3, 3);

    // Set widget up
    openGLwidget = new Widget;
    openGLwidget->setFormat(format);

    openGLwidget->setParent(ui->frame);
    openGLwidget->resize(ui->frame->size());

    QPalette pal = palette();

    QColor col = QColor(128,128,128);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorWall->setPalette(pal);
    openGLwidget->setElementColor("wall",col);


    col = QColor(220,220,220);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorFloor->setPalette(pal);
    openGLwidget->setElementColor("floor",col);


    col = QColor(139,69,19);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorDoor->setPalette(pal);
    openGLwidget->setElementColor("door",col);


    col = QColor(255,215,0);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorTreasure->setPalette(pal);
    openGLwidget->setElementColor("treasure", col);


    col = QColor(255,0,0);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorMonster->setPalette(pal);
    openGLwidget->setElementColor("monster",col);


    col = QColor(0,0,0);
    pal.setColor(QPalette::Window, col);
    ui->widgetColorTrap->setPalette(pal);
    openGLwidget->setElementColor("trap",col);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, QString("About Dungeon Generator"),
            tr("<p style=\"text-align: center;\"><img style=\"display: block; margin-left: auto; margin-right: auto;\" "
                    "src=\"../../img/dungen_logo2.png\" alt=\"DunGen\" height=250 /></p>"
               "<p style=\"text-align: center;\" style=\"font-size:22px\">Dungeon Generator.</p>"
               "<p align=\"justify\"> Project implemented for course of agile methodologies of software development. "
               "The project has been developed using unit tests, TTD and pair programing.</p>"
               "<p style=\"text-align: center;\">Copyright:<br><b>SSW Company ©<b> Wrocław 2016-2017<br>"
               "by:<br>"
               "<font color=\"#194D80\" style=\"font-size:16px\">Adam Sawicki</font><br>"
               "<font color=\"#194D80\" style=\"font-size:16px\">Piotr Szymajda</font><br>"
               "<font color=\"#194D80\" style=\"font-size:16px\">Jakub Wosiak</font><br>"
               "</p>"));
}


bool Ui::alwaysTrue()
{
    return true;
}

void MainWindow::on_actionQuit_triggered()
{
    this->close();
    //exit(0);
}

void MainWindow::on_generateButton_clicked()
{
    if(!map.isEmpty())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Information", "Do you want to overwrite current map?",
                                      QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes)
        {
            map.cleanUp();
        }
        else
        {
            return;
        }
    }

    GeneratorAttributes attributes;
    attributes.setMaxWidth(ui->maxWidthInput->value());
    attributes.setMinWidth(ui->minWidthInput->value());

    attributes.setMaxHeight(ui->maxHeightInput->value());
    attributes.setMinHeight(ui->minHeigthInput->value());

    attributes.setRoomsNumber(ui->roomNumberInput->value());

    attributes.setGenerateAttachments(ui->generateAttachmentsCB->isChecked());

    map.generate(attributes);

    drawTextMap();
    openGLwidget->drawMap(&map);
}

void MainWindow::on_minWidthInput_valueChanged(int minValue)
{
    int maxValue = ui->maxWidthInput->value();
    if(minValue > maxValue)
    {
        ui->maxWidthInput->setValue(minValue);
    }
}

void MainWindow::on_maxWidthInput_valueChanged(int maxValue)
{
    int minValue = ui->minWidthInput->value();
    if(maxValue < minValue)
    {
        ui->minWidthInput->setValue(maxValue);
    }
}

void MainWindow::on_minHeigthInput_valueChanged(int minValue)
{
    int maxValue = ui->maxHeightInput->value();
    if(minValue > maxValue)
    {
        ui->maxHeightInput->setValue(minValue);
    }
}

void MainWindow::on_maxHeightInput_valueChanged(int maxValue)
{
    int minValue = ui->minHeigthInput->value();
    if(maxValue < minValue)
    {
        ui->minHeigthInput->setValue(maxValue);
    }
}

void MainWindow::on_actionSave_file_as_triggered()
{
    if(map.isEmpty())
    {
        QMessageBox::information(this, tr("Error"),
                                 tr("Please generate map before saving")
                                 );
    }
    else
    {
        QString fileName = QFileDialog::getSaveFileName(this,
                                                        tr("Save Current Map"),
                                                        "./untitled.xml",
                                                        tr("XML files (*.xml);;Text files (*.txt)"),
                                                        nullptr,
                                                        QFileDialog::DontUseNativeDialog);


        QString fileType = "xml";

        if(fileName.isEmpty())
        {
            return;
        }

        if(fileName.length() > 4)
        {
            if(fileName.right(4) != QString(".txt") && fileName.right(4) != QString(".xml"))
            {
                fileName+=QString(".xml");
            }
            else
            {
                if(fileName.right(4) == QString(".txt"))
                {
                    fileType = "txt";
                }
            }
        }
        else
        {
            fileName+=QString(".xml");
        }

        fileName.replace(" ", "_");

        QFile file(fileName);

        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }

        if(fileType=="xml")
        {
            QXmlStreamWriter xmlStream(&file);

            xmlStream.setAutoFormatting(true);

            xmlStream.writeStartDocument();

            map.writeToXml(xmlStream);

            xmlStream.writeEndDocument();
        }
        else
        {
            QTextStream textStream(&file);
            std::vector<std::string> textVec = map.mapString();
            for(auto l : textVec)
            {
                textStream << l.c_str() << "\n";
            }
        }

        file.close();

    }
}

void MainWindow::on_actionClear_map_triggered()
{
    if(!map.isEmpty())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Information",
                                      "Do you want to clean current map?",
                                      QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes)
        {
            clearAll();
        }
        else
        {
            return;
        }
    }
}

void MainWindow::on_zoomButtonPlus_clicked()
{
    qreal fontPointSize = ui->mapDebug->fontPointSize();
    if(fontPointSize > 30)
    {
        return;
    }
    else
    {
        ui->mapDebug->setFontPointSize(fontPointSize+1);
        ui->mapDebug->setPlainText(ui->mapDebug->toPlainText());
    }
}

void MainWindow::on_zoomButtonMinus_clicked()
{
    qreal fontPointSize = ui->mapDebug->fontPointSize();
    if(fontPointSize < 5)
    {
        return;
    }
    else
    {
        ui->mapDebug->setFontPointSize(fontPointSize-1);
        ui->mapDebug->setPlainText(ui->mapDebug->toPlainText());
    }
}

void MainWindow::on_actionGenerate_map_triggered()
{
    on_generateButton_clicked();
}


void MainWindow::on_actionZoom_in_2_triggered()
{
    on_zoomButtonPlus_clicked();
}


void MainWindow::on_actionZoom_out_triggered()
{
    on_zoomButtonMinus_clicked();
}

void MainWindow::on_actionZoom_in_triggered()
{
    on_zoomButtonPlus_clicked();
}

void MainWindow::on_actionOpen_file_triggered()
{
    if(!map.isEmpty())
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::warning(this, "Warning",
                                     "Map is already loaded. <br>Do you want to continue and load new map from file?",
                                      QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::No)
        {
            return;
        }
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Map File"),
                                                    ".",
                                                    tr("XML files (*.xml);;Text files (*.txt)"),
                                                    nullptr,
                                                    QFileDialog::DontUseNativeDialog);

    // no file has been selected
    if(fileName == "")
    {
        return;
    }

//    qDebug() << fileName;

    QString extension = fileName.right(4);

    if(extension != ".txt" && extension != ".xml")
    {
        QMessageBox::critical(this, "Error", "This shouldn't happen...", QMessageBox::Ok);
        return;
    }

    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this,
                                 tr("Unable to open file"),
                                 file.errorString());
        return;
    }

    map.cleanUp();

    if( extension == ".xml")
    {
        parseXML(file);
        drawTextMap();
        openGLwidget->drawMap(&map);
        return;
    }

    if( extension == ".txt")
    {
        QMessageBox::critical(this, "Error", "Not implemented yet...", QMessageBox::Ok);
    }
}

void MainWindow::parseXML(QFile &file)
{
    QXmlStreamReader xmlStream(&file);

    QXmlStreamReader::TokenType token = xmlStream.readNext();

    if(token != QXmlStreamReader::StartDocument)
        xmlStream.raiseError("Line:" + QString::number(xmlStream.lineNumber()) + " Wrong XML beginning. Expected: StartDocument");

    token = xmlStream.readNext();

    if(token != QXmlStreamReader::StartElement)
        xmlStream.raiseError("Line:" + QString::number(xmlStream.lineNumber()) + " Wrong type of token - " + xmlStream.tokenString() + ". Expected: StartElement");

    if(xmlStream.name() != "map")
        xmlStream.raiseError("Line:" + QString::number(xmlStream.lineNumber()) + " Wrong token - " + xmlStream.name().toString() + ". Expected: map");

    if( ! xmlStream.hasError() )
    {
        map.parseXML(xmlStream);
    }

    if( xmlStream.hasError() )
    {
        QMessageBox::critical(this, "Error", "An error occured while parsing this XML document:\n" + xmlStream.errorString(), QMessageBox::Ok);

        //qDebug() << "Errors: "<< xmlStream.errorString();

        clearAll();
    }
}

void MainWindow::clearAll()
{
    map.cleanUp();
    openGLwidget->cleanWidget();
    ui->mapDebug->clear();

}

void MainWindow::drawTextMap()
{
    ui->mapDebug->clear();
    QString text;

    std::vector<std::string> debugMap = map.mapString();

    for(unsigned int i=0; i<debugMap.size(); i++)
    {
        text+=QString(debugMap[i].c_str());
        text+="\n";
    }
    ui->mapDebug->setPlainText(text);
}
