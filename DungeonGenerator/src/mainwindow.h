#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include "dungeonmap.h"

namespace Ui {
    bool alwaysTrue();
    class MainWindow;
}

class Widget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class guiTest;

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionAbout_triggered();

    void on_actionQuit_triggered();

    void on_generateButton_clicked();

    void on_minWidthInput_valueChanged(int minValue);

    void on_maxWidthInput_valueChanged(int maxValue);

    void on_minHeigthInput_valueChanged(int minValue);

    void on_maxHeightInput_valueChanged(int maxValue);

    void on_actionSave_file_as_triggered();

    void on_actionClear_map_triggered();

    void on_zoomButtonPlus_clicked();

    void on_zoomButtonMinus_clicked();

    void on_actionGenerate_map_triggered();

    void on_actionZoom_in_2_triggered();

    void on_actionZoom_out_triggered();

    void on_actionZoom_in_triggered();

    void on_actionOpen_file_triggered();

protected:
    Ui::MainWindow *ui;
    DungeonMap      map;
    Widget  *openGLwidget;

    void    drawTextMap();
    void    parseXML(QFile &file);

    void    clearAll();
};



#endif // MAINWINDOW_H
