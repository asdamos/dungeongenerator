#include "room.h"
#include <iostream>
#include <QString>
#include <algorithm>
#include <QDebug>
#include <cmath>

bool Room::isColliding(Room *r)
{
    if ( r == nullptr )
    {
        return false;
    }

    if (this == r)
    {
        return false;
    }

    return !((getXend() <= r->x) || (x >= r->getXend()) ||
             (getYend() <= r->y) || (y >= r->getYend()));
}


bool Room::containsField(int xPos, int yPos)
{
    return !(xPos < getX() || xPos > getXend() || yPos < getY() || yPos > getYend());
}


bool Room::hasCornerAt(int xPos, int yPos)
{
    return (xPos == getX() || xPos == getXend()) && (yPos == getY() || yPos == getYend());
}


Field* Room::getField(int xPos, int yPos)
{
    Field* result = nullptr;
    if (containsField(xPos, yPos))
    {
        result = fields[xPos - getX()][yPos - getY()];
    }
    return result;
}


bool Room::operator==(const Room &r)
{
    return r.getHeight() == height && r.getWidth() == width && r.getX() == x && r.getY() == y;
}

void Room::writeToXml(QXmlStreamWriter &xmlStream)
{
    QString number;

    xmlStream.writeStartElement("room");

    number.setNum(getX());

    xmlStream.writeAttribute("x", number);

    number.setNum(getY());
    xmlStream.writeAttribute("y", number);

    number.setNum(getWidth());

    xmlStream.writeAttribute("width", number);

    number.setNum(getHeight());
    xmlStream.writeAttribute("height", number);



    for (int i = 0; i < width; i++){
        for (int j = 0; j < height; j++){

            xmlStream.writeStartElement("field");


            number.setNum(i);
            xmlStream.writeAttribute("x", number);

            number.setNum(j);
            xmlStream.writeAttribute("y", number);

            fields[i][j]->writeToXml(xmlStream);


            xmlStream.writeEndElement();

        }
    }

    xmlStream.writeEndElement();
}

bool Room::parseXML(QXmlStreamReader &xmlStream)
{
    QXmlStreamAttributes atr;

    for (int i = 0; i < width; i++){
        for (int j = 0; j < height; j++){
            QXmlStreamReader::TokenType token = xmlStream.readNext();
            token = xmlStream.readNext();

            if(token != QXmlStreamReader::StartElement) xmlStream.raiseError("Wrong type of token - " + xmlStream.tokenString() + ". Expected: StartElement");

            atr = xmlStream.attributes();
            int x = atr.value("x").toInt();
            int y = atr.value("y").toInt();

            if( fields[x][y] != nullptr )
            {
                xmlStream.raiseError("Duplicated field - x:" + QString::number(x) + ", y:" + QString::number(y));
                return false;
            }

            token = xmlStream.readNext();
            token = xmlStream.readNext();

            if(token != QXmlStreamReader::StartElement) xmlStream.raiseError("Wrong type of token - " + xmlStream.tokenString() + ". Expected: StartElement");

            if( xmlStream.name() != "type" )
            {
                xmlStream.raiseError("Wrong token - \"" + xmlStream.name().toString() + "\". Expected: type");
                return false;
            }

            QString type = xmlStream.readElementText();

            if(type == "wall")
            {
                fields[x][y] = new Wall();
            }
            else if(type == "floor")
            {
                Floor* floor= new Floor();
                //Read attachment type
                token = xmlStream.readNext();
                token = xmlStream.readNext();

                if( xmlStream.name() != "attachment" )
                {
                    xmlStream.raiseError("Wrong token - \"" + xmlStream.name().toString() + "\". Expected: attachment");
                    return false;
                }

                QString type = xmlStream.readElementText();

                if(type == "monster")
                {
                    floor->setAttachment(new Monster());
                }
                else if(type == "treasure")
                {
                    floor->setAttachment(new Treasure());
                }
                else if(type == "trap")
                {
                    floor->setAttachment(new Trap());
                }
                else
                {
                    // nothing or unknown type
                    if(type != "nothing")
                    {
                        xmlStream.raiseError("Unknown attachment type - \"" + type + "\". Expected: monster or treasure or trap");
                    }
                }

                fields[x][y] = floor;

            }
            else if (type == "door")
            {
                fields[x][y] = new Door();
            }
            else
            {
                xmlStream.raiseError("Unknown field type - \"" + type + "\". Expected: wall or floor or door");
                return false;
            }


            // now parse tags ends :
            token = xmlStream.readNext();
            token = xmlStream.readNext();
        }
    }

    return true;
}

int Room::distance(Room *r)
{
    float dX = r->getX() - getX();
    float dY = r->getY() - getY();

    return int(sqrt(dX * dX + dY * dY));
}

Room::Room(int x, int y, int width, int height, bool generate_fields)
    : x(x)
    , y(y)
    , width(width)
    , height(height)
{
    if (width <= 0)
        throw *new std::string("Width value must be greater than 0!");

    if (height <= 0)
        throw *new std::string("Height value must be greater than 0!");


    fields = new Field** [width];

    for (int i = 0; i < width; i++){
        fields[i] = new Field* [height];
        for (int j = 0; j < height; j++){
            fields[i][j] = nullptr;
        }
    }

    if( generate_fields )
        generateRoom();

}

Room::~Room()
{
    for (int i = 0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            delete fields[i][j];
        }
        delete fields[i];
    }
    delete fields;

}


void Room::generateRoom()
{

    for (int i = 0; i < width; i++){
        for (int j = 0; j < height; j++)
        {
            if (i == 0 || i == width-1 || j == 0 || j == height-1)
                fields[i][j] = new Wall();
            else
                fields[i][j] = new Floor();
        }
    }
}
