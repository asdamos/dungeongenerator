#ifndef ROOM_H
#define ROOM_H

#include "field.h"
#include <QXmlStreamWriter>

class Field;

class Room{
    int          x;
    int          y;
    int          width;
    int          height;
public:
    Field ***    fields;

public:
    Room(int x, int y, int width, int height, bool generate_fields = true);
    ~Room();

    int getX() const { return x; }
    int getY() const { return y; }
    int getXend() const { return x + width - 1; }
    int getYend() const { return y + height - 1; }
    int getWidth() const { return width; }
    int getHeight() const { return height; }

    bool operator==(const Room &r);
    bool isColliding(Room* r);
    bool containsField(int xPos, int yPos);
    bool hasCornerAt(int xPos, int yPos);
    Field* getField(int xPos, int yPos);

    void writeToXml(QXmlStreamWriter &xmlStream);
    bool parseXML(QXmlStreamReader & xmlStream);

    int getField() { return width * height; }
    int distance(Room* r);

private:
    void generateRoom();

};


#endif // ROOM_H
