#version 330
layout(location = 0) in vec3 position;
out vec4 vColor;

uniform mat4 modelToWorld;
uniform mat4 worldToView;

uniform vec3 color;

void main()
{
  gl_Position = worldToView * modelToWorld * vec4(position, 1.0);
  vColor = vec4(color, 1.0f);
}
