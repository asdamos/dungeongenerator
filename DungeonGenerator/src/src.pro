
QT += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
CONFIG -= app_bundle

TEMPLATE = app

TARGET = DungeonGenerator

SOURCES += main.cpp \
    mainwindow.cpp \
    generatorattributes.cpp \
    field.cpp \
    room.cpp \
    dungeonmap.cpp \
    floorattachment.cpp \
    transform3d.cpp \
    widget.cpp

FORMS += \
    mainwindow.ui

HEADERS += \
    mainwindow.h \
    generatorattributes.h \
    field.h \
    room.h \
    dungeonmap.h \
    floorattachment.h \
    widget.h \
    transform3d.h

RESOURCES += \
    resources.qrc
