#include "widget.h"
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QExposeEvent>
#include "dungeonmap.h"
#include "floorattachment.h"
#include <assert.h>

static const QVector2D DG_demo[] = {
    // D letter
    QVector2D(-4.0f, 2.0f),
    QVector2D(-4.0f, -3.0f),
    QVector2D(-3.0f, -3.0f),
    QVector2D(-3.0f, -3.0f),
    QVector2D(-3.0f, 2.0f),
    QVector2D(-4.0f, 2.0f),

    QVector2D(-3.0f, -2.0f),
    QVector2D(-3.0f, -3.0f),
    QVector2D(-1.0f, -3.0f),
    QVector2D(-1.0f, -3.0f),
    QVector2D(-1.0f, -2.0f),
    QVector2D(-3.0f, -2.0f),

    QVector2D(-1.0f, -2.0f),
    QVector2D(-0.1f, -2.0f),
    QVector2D(-1.0f, 0.0f),
    QVector2D(-0.1f,  0.0f),
    QVector2D(-1.0f, 0.0f),
    QVector2D(-0.1f, -2.0f),

    QVector2D(-2.0f, 0.0f),
    QVector2D(-1.0f, 0.0f),
    QVector2D(-2.0f, 1.0f),
    QVector2D(-1.0f, 1.0f),
    QVector2D(-2.0f, 1.0f),
    QVector2D(-1.0f, 0.0f),

    QVector2D(-3.0f, 1.0f),
    QVector2D(-2.0f, 1.0f),
    QVector2D(-3.0f, 2.0f),
    QVector2D(-2.0f, 2.0f),
    QVector2D(-3.0f, 2.0f),
    QVector2D(-2.0f, 1.0f),

    // G letter
    QVector2D(1.0f, 1.0f),
    QVector2D(3.0f, 1.0f),
    QVector2D(3.0f, 2.0f),
    QVector2D(1.0f, 2.0f),
    QVector2D(1.0f, 1.0f),
    QVector2D(3.0f, 2.0f),

    QVector2D(0.1f, 1.0f),
    QVector2D(1.0f, -2.0f),
    QVector2D(1.0f, 1.0f),
    QVector2D(0.1f, -2.0f),
    QVector2D(1.0f, -2.0f),
    QVector2D(0.1f, 1.0f),

    QVector2D(1.0f, -2.0f),
    QVector2D(3.0f, -3.0f),
    QVector2D(3.0f, -2.0f),
    QVector2D(1.0f, -2.0f),
    QVector2D(1.0f, -3.0f),
    QVector2D(3.0f, -3.0f),

    QVector2D(3.0f, 0.0f),
    QVector2D(4.0f, -2.0f),
    QVector2D(4.0f, 0.0f),
    QVector2D(3.0f, -2.0f),
    QVector2D(4.0f, -2.0f),
    QVector2D(3.0f, 0.0f),

    QVector2D(3.0f, -1.0f),
    QVector2D(3.0f, 0.0f),
    QVector2D(2.0f, -1.0f),
    QVector2D(3.0f, 0.0f),
    QVector2D(2.0f, 0.0f),
    QVector2D(2.0f, -1.0f),
};

//static const QVector2D square[] = {
//        QVector2D(-1.0f, -1.0f),
//        QVector2D(1.0f, -1.0f),
//        QVector2D(-1.0f, 1.0f),
//        QVector2D(1.0f, 1.0f),
//        QVector2D(-1.0f, 1.0f),
//        QVector2D(1.0f, -1.0f)
//};

/*******************************************************************************
 * OpenGL Events
 ******************************************************************************/

Widget::Widget()
{
    // z cord is for "zoom"

    scale = -10.0f;
    deltaScale = 1.0f;
    deltaSpeed = 0.002f;

    x = 0.0f;
    y = 0.0f;
    transformMatrix.translate(x, y, scale);

    demo = true;
    demo_turn = true;
    setMouseTracking(true);

    demoColor = QVector3D(1.0f, 0.0f, 0.0f);

    elements2draw.emplace_back("wall");
    elements2draw.emplace_back("floor");
    elements2draw.emplace_back("door");
    elements2draw.emplace_back("treasure");
    elements2draw.emplace_back("trap");
    elements2draw.emplace_back("monster");

    setElementColor("wall", QVector3D(1.0f, 1.0f, 0.0f));
    setElementColor("floor", QVector3D(0.0f, 1.0f, 0.5f));
    setElementColor("door", QVector3D(1.0f, 0.0f, 0.5f));

    setElementColor("treasure", QVector3D(1.0f, 1.0f, 0.5f));
    setElementColor("trap",     QVector3D(0.5f, 1.0f, 0.5f));
    setElementColor("monster",  QVector3D(0.0f, 0.5f, 1.0f));
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    // Initialize OpenGL Backend
    initializeOpenGLFunctions();
    connect(context(), SIGNAL(aboutToBeDestroyed()), this, SLOT(teardownGL()), Qt::DirectConnection);
    connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));
    //printVersionInformation();

    // Set global information
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    // Application-specific initialization
    {
        // Create Shader (Do not release until VAO is created)
        shaderProgram = new QOpenGLShaderProgram();
        shaderProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/simple.vert");
        shaderProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/simple.frag");
        shaderProgram->link();
        shaderProgram->bind();

        // Cache Uniform Locations
        uniformModelToWorld = shaderProgram->uniformLocation("modelToWorld");
        uniformWorldToView = shaderProgram->uniformLocation("worldToView");
        uniformColor = shaderProgram->uniformLocation("color");


        // Create Buffer (Do not release until VAO is created)
        vbo.create();
        vbo.bind();
        vbo.setUsagePattern(QOpenGLBuffer::DynamicDraw);
        vbo.allocate(DG_demo, sizeof(DG_demo));

//        qDebug() << "demo_vertex bufferId =" << demo_vertex.bufferId();

        // Create Vertex Array Object
        vao.create();
        vao.bind();
        shaderProgram->enableAttributeArray(0);
        shaderProgram->setAttributeArray(0, GL_FLOAT, 0, 2);

        vbo.release();
        shaderProgram->release();
    }
}

void Widget::resizeGL(int width, int height)
{
    projectionMatrix.setToIdentity();
    projectionMatrix.perspective(45.0f, width / float(height), 0.0f, 1000.0f);
}

void Widget::paintGL()
{
    // Clear
    glClear(GL_COLOR_BUFFER_BIT);

    // Render using our shader
    shaderProgram->bind();
    shaderProgram->setUniformValue(uniformWorldToView, projectionMatrix);
    shaderProgram->setUniformValue(uniformModelToWorld, transformMatrix.toMatrix());

    if(demo)
    {
        shaderProgram->setUniformValue(uniformColor, demoColor);
        glDrawArrays(GL_TRIANGLES, 0, 60);
    }
    else
    {
        int startPos = 0;

        for(auto e : elements2draw)
        {
            shaderProgram->setUniformValue(uniformColor, e.color);
            glDrawArrays(GL_TRIANGLES, startPos, e.dataSize);
            startPos += e.dataSize;
        }
    }

    shaderProgram->release();
}

void Widget::wheelEvent(QWheelEvent *event)
{
    QPoint numDegrees = event->angleDelta() / 8;

    if (!numDegrees.isNull())
    {

        QPoint numSteps = numDegrees / 15;
        float scaleChange = deltaScale * numSteps.y();

        if(scale + scaleChange < -2.0f /*&& scale + scaleChange > -100.0f*/ )
        {
            scale += scaleChange;
            transformMatrix.translate(QVector3D( 0.0f, 0.0f, scaleChange));
        }
    }

    event->accept();
}

void Widget::drawMap(DungeonMap *dm)
{
    demo = false;
    //reset translation and rotation
    transformMatrix.setTranslation(QVector3D(0.0f, 0.0f, scale));
    transformMatrix.setRotation(0.0f,0.0f,0.0f,0.0f);

    //rotate map
    transformMatrix.rotate(180.0f, QVector3D(1.0f,0.0f,0.0f));

    std::vector <QVector2D> wallTriangles = dm->getWallsTriangles();
    int wallsDataSize = wallTriangles.size();
    assert(setElementDataSize("wall", wallsDataSize));

    std::vector <QVector2D> roomTriangles = dm->getRoomsTriangles();
    int roomsDataSize = roomTriangles.size();
    assert(setElementDataSize("floor", roomsDataSize));

    std::vector <QVector2D> doorsTriangles = dm->getDoorTriangles();
    int doorsDataSize = doorsTriangles.size();
    assert(setElementDataSize("door", doorsDataSize));

    std::vector <QVector2D> monsterTriangles = dm->getAttachmentTriangles(attachmentType::MONSTER);
    int monsterDataSize = monsterTriangles.size();
    assert(setElementDataSize("monster", monsterDataSize));

    std::vector <QVector2D> trapTriangles = dm->getAttachmentTriangles(attachmentType::TRAP);
    int trapDataSize = trapTriangles.size();
    assert(setElementDataSize("trap", trapDataSize));

    std::vector <QVector2D> treasureTriangles = dm->getAttachmentTriangles(attachmentType::TREASURE);
    int treasureDataSize = treasureTriangles.size();
    assert(setElementDataSize("treasure", treasureDataSize));

    std::vector <QVector2D> allTriangles;
    allTriangles.reserve(wallsDataSize + roomsDataSize + doorsDataSize + monsterDataSize + trapDataSize + treasureDataSize);
    allTriangles.insert( allTriangles.end(), wallTriangles.begin(),     wallTriangles.end()     );
    allTriangles.insert( allTriangles.end(), roomTriangles.begin(),     roomTriangles.end()     );
    allTriangles.insert( allTriangles.end(), doorsTriangles.begin(),    doorsTriangles.end()    );
    allTriangles.insert( allTriangles.end(), monsterTriangles.begin(),  monsterTriangles.end()  );
    allTriangles.insert( allTriangles.end(), trapTriangles.begin(),     trapTriangles.end()     );
    allTriangles.insert( allTriangles.end(), treasureTriangles.begin(), treasureTriangles.end() );


//    for(auto t : allTriangles)
//    {
//        qDebug() << t;
//    }

    vbo.bind();
    vbo.allocate(allTriangles.data(), allTriangles.size()*sizeof(QVector2D));

}

void Widget::cleanWidget()
{
    demo = true;
    scale = -10.0f;
    transformMatrix.setTranslation(QVector3D(0.0f, 0.0f, scale));
    transformMatrix.rotate(180.0f, QVector3D(1.0f,0.0f,0.0f));

    vbo.bind();
    vbo.allocate(DG_demo, sizeof(DG_demo));

}

bool Widget::setElementDataSize(QString name, int size)
{
    for(auto &&e : elements2draw)
    {
        if(e.name == name)
        {
            e.dataSize = size;
            return true;
        }
    }
    return false;
}

bool Widget::setElementColor(QString name, QVector3D color)
{
    for(auto &&e : elements2draw)
    {
        if(e.name == name)
        {
            e.color = color;
            return true;
        }
    }
    return false;
}

bool Widget::setElementColor(QString name, QColor color)
{
    for(auto &&e : elements2draw)
    {
        if(e.name == name)
        {
            QVector3D newColor(color.red()/255.0f, color.green()/255.0f, color.blue()/255.0f );
            e.color = newColor;
            return true;
        }
    }
    return false;
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    float dx = (event->x() - lastPos.x()) * deltaSpeed * (-scale-2.0f);

    float dy = -(event->y() - lastPos.y()) * deltaSpeed * (-scale-2.0f);

    if (event->buttons() & Qt::LeftButton)
    {
        transformMatrix.translate(QVector3D(dx, dy, 0.0f));
    }

    x += dx;
    y += dy;

    lastPos = event->pos();
}

void Widget::teardownGL()
{
    // Actually destroy our OpenGL information
    vao.destroy();
    vbo.destroy();
    delete shaderProgram;
}


#define ANIM_SPEED 0.02f
// if you have to do sth befor every frame draw put it here
void Widget::update()
{
    if(demo)
    {
        transformMatrix.rotate(1.0f, QVector3D(0.0f, 0.1f, 0.01f));
        if(demo_turn)
        {
            demoColor += QVector3D(-ANIM_SPEED, ANIM_SPEED, 0.0f);

            if(demoColor.x() <= 0.0f){
                demo_turn = !demo_turn;
            }
        }
        else
        {
            demoColor += QVector3D(ANIM_SPEED, -ANIM_SPEED, 0.0f);

            if(demoColor.y() <= 0.0f){
                demo_turn = !demo_turn;
            }
        }
    }

    // Schedule a redraw
    QOpenGLWidget::update();
}

//void Widget::exposeEvent(QExposeEvent *ev)
//{
//  if (ev->region() != m_cachedRegion)
//  {
//    m_cachedRegion = ev->region();
//    QOpenGLWidget::exposeEvent(ev);
//  }
//  else
//  {
//    ev->ignore();
//  }

//}

/*******************************************************************************
 * Private Helpers
 ******************************************************************************/

void Widget::printVersionInformation()
{
    QString glType;
    QString glVersion;
    QString glProfile;

    // Get Version Information
    glType = (context()->isOpenGLES()) ? "OpenGL ES" : "OpenGL";
    glVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));

    // Get Profile Information
#define CASE(c) case QSurfaceFormat::c: glProfile = #c; break
    switch (format().profile())
    {
    CASE(NoProfile);
    CASE(CoreProfile);
    CASE(CompatibilityProfile);
    }
#undef CASE

    // qPrintable() will print our QString w/o quotes around it.
    qDebug() << qPrintable(glType) << qPrintable(glVersion) << "(" << qPrintable(glProfile) << ")";
}

mapElement::mapElement(QString name_)
    :dataSize(0)
    ,color(QVector3D(0.0f,0.0f,0.0f))
    ,name(name_)
{

}
