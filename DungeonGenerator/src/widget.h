#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include "transform3d.h"

class QExposeEvent;
class QOpenGLShaderProgram;
class DungeonMap;

struct mapElement{
    int         dataSize;
    QVector3D   color;
    QString     name;

    mapElement(QString name_);
};

class Widget : public QOpenGLWidget,
        protected QOpenGLFunctions
{
    Q_OBJECT

    // OpenGL Events
public:

    Widget();
    ~Widget();
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void drawMap(DungeonMap *dm);
    void cleanWidget();

    bool setElementDataSize(QString name, int size);
    bool setElementColor(QString name, QVector3D color);
    bool setElementColor(QString name, QColor color);

public slots:
    void teardownGL();
protected slots:
    void update();

private:
    // OpenGL State Information
    QOpenGLBuffer             vbo;
    QOpenGLVertexArrayObject  vao;
    QOpenGLShaderProgram      *shaderProgram;

    std::vector <mapElement>  elements2draw;

    // Shader Information
    int                         uniformModelToWorld;
    int                         uniformWorldToView;
    int                         uniformColor;

    QMatrix4x4                  projectionMatrix;
    Transform3D                 transformMatrix;

    // Private Helpers
    void printVersionInformation();

    // Transformation variables
    float           deltaScale;
    float           scale;

    float           deltaSpeed;
    float           x, y;

    QPoint          lastPos;

    // other
    bool            demo;
    bool            demo_turn;
    QVector3D       demoColor;
};

#endif // WIDGET_H
