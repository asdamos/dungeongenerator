#include <QtTest>
#include <QCoreApplication>
#include <QtTest/QtTest>
#include "../dungeonMapClassTest/../../src/field.h"
#include "../dungeonMapClassTest/../../src/room.h"
#include "../dungeonMapClassTest/../../src/dungeonmap.h"
// add necessary includes here

class dungeonMapClassTest : public QObject
{
    Q_OBJECT

public:
    dungeonMapClassTest();
    ~dungeonMapClassTest();

private slots:
    void initTestCase();
    void cleanupTestCase1();
    void cleanupTestCase2();
    void collideWithAnythingTest1();
    void collideWithAnythingTest2();
    void collideWithAnythingTest3();
    void collideWithAnythingTest4();
    void collideWithAnythingTest5();
    void collideWithAnythingTest6();
    void collideWithAnythingTest7();
    void nextRoomTest1();
    void nextRoomTest2();
    void nextRoomTest3();
    void isEmptyTest1();
    void isEmptyTest2();
};


dungeonMapClassTest::dungeonMapClassTest()
{

}

dungeonMapClassTest::~dungeonMapClassTest()
{

}

void dungeonMapClassTest::initTestCase()
{
}

void dungeonMapClassTest::cleanupTestCase1()
{
    DungeonMap dm;
    dm.cleanUp();

    QCOMPARE(dm.isEmpty(), true);
}


void dungeonMapClassTest::cleanupTestCase2()
{
    DungeonMap dm;
    unsigned long n = 10;
    GeneratorAttributes conf;
    conf.setMinHeight(3);
    conf.setMinWidth(3);
    conf.setMaxHeight(5);
    conf.setMaxWidth(5);
    conf.setRoomsNumber(n);

    dm.generate(conf);


    QCOMPARE(dm.isEmpty(), false);

    dm.cleanUp();

    QCOMPARE(dm.isEmpty(), true);
}



void dungeonMapClassTest::collideWithAnythingTest1()
{
    DungeonMap dm;

    Room r1(0, 0, 5, 5);
    dm.rooms.push_back(&r1);

    Room r2(3, 2, 20, 20);

    QCOMPARE(dm.collideWithAnything(r2), true);
}

void dungeonMapClassTest::collideWithAnythingTest2()
{
    DungeonMap dm;

    Room r1(0, 0, 5, 5);
    dm.rooms.push_back(&r1);

    Room r2(5, 3, 20, 20);

    QCOMPARE(dm.collideWithAnything(r2), false);
}

void dungeonMapClassTest::collideWithAnythingTest3()
{
    DungeonMap dm;

    Room r1(0, 0, 3, 3);
    dm.rooms.push_back(&r1);

    Room r2(0, 3, 3, 3);
    dm.rooms.push_back(&r2);

    Room r3(3, 0, 3, 3);
    dm.rooms.push_back(&r3);

    Room r4(3, 3, 3, 3);
    dm.rooms.push_back(&r4);

    Room r5(5, 2, 20, 20);

    QCOMPARE(dm.collideWithAnything(r5), false);
}

void dungeonMapClassTest::collideWithAnythingTest4()
{
    DungeonMap dm;

    Room r1(0, 0, 3, 3);
    dm.rooms.push_back(&r1);

    Room r2(0, 3, 3, 3);
    dm.rooms.push_back(&r2);

    Room r3(3, 0, 3, 3);
    dm.rooms.push_back(&r3);

    Room r4(3, 3, 3, 3);
    dm.rooms.push_back(&r4);

    Room r5(30, 20, 20, 20);

    QCOMPARE(dm.collideWithAnything(r5), false);
}


void dungeonMapClassTest::collideWithAnythingTest5()
{
    DungeonMap dm;

    Room r1(0,0,3,5);
    dm.rooms.push_back(&r1);

    Room r2(-3,2,4,5);
    dm.rooms.push_back(&r2);

    Room r3(0,3,3,3);
    dm.rooms.push_back(&r3);

    QCOMPARE(dm.collideWithAnything(r3), true);
}


void dungeonMapClassTest::collideWithAnythingTest6()
{
    DungeonMap dm;

    Room r1(0,0,5,5);
    dm.rooms.push_back(&r1);

    Room r2(2,4,4,3);
    dm.rooms.push_back(&r2);

    Room r3(4,1,3,4);
    dm.rooms.push_back(&r3);

    bool result = dm.collideWithAnything(r1) ||
                  dm.collideWithAnything(r2) ||
                  dm.collideWithAnything(r3);

    QCOMPARE(result, false);
}


void dungeonMapClassTest::collideWithAnythingTest7()
{
    DungeonMap dm;

    Room r1(0,0,4,3);
    dm.rooms.push_back(&r1);

    Room r2(1,2,4,3);
    dm.rooms.push_back(&r2);

    Room r3(-3,1,4,5);
    dm.rooms.push_back(&r3);

    Room r4(-2,3,4,3);
    dm.rooms.push_back(&r4);

    bool result = false;

    for (auto r : dm.rooms)
    {
        result |= dm.collideWithAnything(*r);
    }

    QCOMPARE(result, true);
}


void dungeonMapClassTest::nextRoomTest1()
{
    DungeonMap dm;
    GeneratorAttributes conf;
    conf.setMinHeight(3);
    conf.setMinWidth(3);
    conf.setMaxHeight(5);
    conf.setMaxWidth(5);
    conf.setRoomsNumber(1);

    dm.config = conf;

    dm.nextRoom();

    QCOMPARE(dm.rooms.size(), (unsigned long) 1);

    bool anyCollision = false;
    for (Room* r : dm.rooms)
    {
        anyCollision |= dm.collideWithAnything(*r);
    }

    QCOMPARE(anyCollision, false);
}

void dungeonMapClassTest::nextRoomTest2()
{
    DungeonMap dm;
    unsigned long n = 10;
    GeneratorAttributes conf;
    conf.setMinHeight(3);
    conf.setMinWidth(3);
    conf.setMaxHeight(5);
    conf.setMaxWidth(5);
    conf.setRoomsNumber(n);

    dm.config = conf;

    for (unsigned long i = 0; i < n; i++)
    {
        dm.nextRoom();
    }

    QCOMPARE(dm.rooms.size(), n);

    bool anyCollision = false;
    for (Room* r : dm.rooms)
    {
        anyCollision |= dm.collideWithAnything(*r);
    }

    QCOMPARE(anyCollision, false);
}

void dungeonMapClassTest::nextRoomTest3()
{
    DungeonMap dm;
    unsigned long n = 100;
    GeneratorAttributes conf;
    conf.setMinHeight(3);
    conf.setMinWidth(3);
    conf.setMaxHeight(5);
    conf.setMaxWidth(5);
    conf.setRoomsNumber(n);

    dm.config = conf;

    for (unsigned long i = 0; i < n; i++)
    {
        dm.nextRoom();
    }

    QCOMPARE(dm.rooms.size(), n);

    bool anyCollision = false;
    for (Room* r : dm.rooms)
    {
        anyCollision |= dm.collideWithAnything(*r);
    }

    QCOMPARE(anyCollision, false);

}



void dungeonMapClassTest::isEmptyTest1()
{
    DungeonMap dm;

    QCOMPARE(dm.isEmpty(), true);
}


void dungeonMapClassTest::isEmptyTest2()
{
    DungeonMap dm;
    GeneratorAttributes conf;
    conf.setMinHeight(3);
    conf.setMinWidth(3);
    conf.setMaxHeight(5);
    conf.setMaxWidth(5);
    conf.setRoomsNumber(1);

    dm.config = conf;

    dm.nextRoom();

    QCOMPARE(dm.isEmpty(), false);
}



QTEST_MAIN(dungeonMapClassTest)

#include "tst_dungeonMapClassTest.moc"
