
QT += testlib gui widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

FORMS += ../../src/mainwindow.ui


SOURCES += tst_guitest.cpp \
    ../../src/mainwindow.cpp \
    ../../src/generatorattributes.cpp \
    ../../src/field.cpp \
    ../../src/room.cpp \
    ../../src/dungeonmap.cpp \
    ../../src/floorattachment.cpp \
    ../../src/widget.cpp \
    ../../src/transform3d.cpp

HEADERS += ../../src/mainwindow.h \
    ../../src/generatorattributes.h \
    ../../src/field.h \
    ../../src/room.h \
    ../../src/dungeonmap.h \
    ../../src/floorattachment.h \
    ../../src/widget.h \
    ../../src/transform3d.h

DEFINES += SRCDIR="$$PWD/../../../src/"

RESOURCES += \
    ../../src/resources.qrc
