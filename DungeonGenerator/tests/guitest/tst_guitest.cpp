#include <QtTest>
#include <QCoreApplication>
#include <QtWidgets>
#include <QtTest/QtTest>
#include "../guitest/../../src/mainwindow.h"
#include "../guitest/../../src/ui_mainwindow.h"

// add necessary includes here

class guiTest : public QObject
{
    Q_OBJECT

public:
    guiTest();
    ~guiTest();

private:
    MainWindow* mw;
    bool msg_box_found;
    bool press_tab_before_enter = false;

private slots:
    void initTestCase();
    void cleanupTestCase();

    void testMapGeneratorButton1();
    void testMapGeneratorButton2();
    void testMapGeneratorButton3();

    void testChangeValue1();

// for non-test methods
protected slots:
    void CloseMessageBoxes();

};

guiTest::guiTest()
{

}

guiTest::~guiTest()
{

}

void guiTest::initTestCase()
{
    mw = new MainWindow;
    mw->show();

    QCOMPARE(mw->map.isEmpty(), true);
    QCOMPARE(mw->ui->mapDebug->toPlainText(), QString(""));
}

void guiTest::cleanupTestCase()
{
    mw->close();
    delete mw;
}

void guiTest::testMapGeneratorButton1()
{
    QTest::mouseMove(mw->ui->generateButton, QPoint(), 200);
    QTest::mouseClick(mw->ui->generateButton, Qt::MouseButton::LeftButton);

    QCOMPARE(mw->map.isEmpty(), false);
    QString output = mw->ui->mapDebug->toPlainText();
    QString clear_output = output.replace('M','.')
                                 .replace('$','.')
                                 .replace('T','.');
    QCOMPARE(clear_output, QString("###\n#.#\n###\n"));
}


void guiTest::CloseMessageBoxes()
{
    msg_box_found = false;

    QWidgetList topWidgets = QApplication::topLevelWidgets();
    foreach (QWidget *w, topWidgets)
    {
        if (QMessageBox *mb = qobject_cast<QMessageBox *>(w))
        {
//            qDebug() << "QMessageBox found";
//            qDebug() << mb->text();
            msg_box_found = true;
            if( press_tab_before_enter )
            {
                QTest::keyClick(mb, Qt::Key_Tab);
            }
            QTest::keyClick(mb, Qt::Key_Enter);
        }
    }
}

void guiTest::testMapGeneratorButton2()
{
    press_tab_before_enter = false;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(CloseMessageBoxes()));
    timer->start(500);

    QTest::mouseMove(mw->ui->generateButton, QPoint(), 200);
    QTest::mouseClick(mw->ui->generateButton, Qt::MouseButton::LeftButton);

    QCOMPARE(msg_box_found, true);
    QCOMPARE(mw->map.isEmpty(), false);
    QString output = mw->ui->mapDebug->toPlainText();
    QString clear_output = output.replace('M','.')
                                 .replace('$','.')
                                 .replace('T','.');
    QCOMPARE(clear_output, QString("###\n#.#\n###\n"));
}

void guiTest::testMapGeneratorButton3()
{
    press_tab_before_enter = true;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(CloseMessageBoxes()));
    timer->start(500);


    mw->ui->minHeigthInput->setValue(4);
    mw->ui->minWidthInput->setValue(5);

    QTest::mouseMove(mw->ui->generateButton, QPoint(), 200);
    QTest::mouseClick(mw->ui->generateButton, Qt::MouseButton::LeftButton);

    QCOMPARE(msg_box_found, true);
    QCOMPARE(mw->map.isEmpty(), false);
    QString output = mw->ui->mapDebug->toPlainText();
    QString clear_output = output.replace('M','.')
                                 .replace('$','.')
                                 .replace('T','.');
    QCOMPARE(clear_output, QString("#####\n#...#\n#...#\n#####\n"));
}

void guiTest::testChangeValue1()
{

    mw->ui->minHeigthInput->setValue(4);
    QCOMPARE(mw->ui->maxHeightInput->value(), 4);

    mw->ui->minWidthInput->setValue(44);
    QCOMPARE(mw->ui->maxWidthInput->value(), 44);

    mw->ui->minWidthInput->setValue(5);
    QCOMPARE(mw->ui->maxWidthInput->value(), 44);


    mw->ui->minWidthInput->setValue(49);
    QCOMPARE(mw->ui->maxWidthInput->value(), 49);

    mw->ui->maxWidthInput->setValue(8);
    QCOMPARE(mw->ui->minWidthInput->value(), 8);



}


QTEST_MAIN(guiTest)

#include "tst_guitest.moc"
