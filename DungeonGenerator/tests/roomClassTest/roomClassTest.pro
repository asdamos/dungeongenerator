
QT += testlib
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

SOURCES +=  tst_roomClassTest.cpp \
    ../../src/generatorattributes.cpp \
    ../../src/field.cpp \
    ../../src/room.cpp \
    ../../src/dungeonmap.cpp \
    ../../src/floorattachment.cpp


HEADERS += ../../src/generatorattributes.h \
    ../../src/field.h \
    ../../src/room.h \
    ../../src/dungeonmap.h \
    ../../src/floorattachment.h


DEFINES += SRCDIR="$$PWD/../../src/"
