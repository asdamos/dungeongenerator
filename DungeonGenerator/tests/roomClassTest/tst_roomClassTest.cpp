#include <QtTest>
#include <QCoreApplication>
#include <QtTest/QtTest>
#include "../roomClassTest/../../src/field.h"
#include "../roomClassTest/../../src/room.h"
#include "../roomClassTest/../../src/dungeonmap.h"
// add necessary includes here

class roomClassTest : public QObject
{
    Q_OBJECT

public:
    roomClassTest();
    ~roomClassTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void testRoomConstruct();
    void testRoomColliding1();
    void testRoomColliding2();
    void testRoomColliding3();
    void testRoomColliding4();
    void testRoomColliding5();
    void testRoomColliding6();
    void testRoomColliding7();

    void testContainsField1();
    void testContainsField2();
    void testContainsField3();

    void testEqualityOperator1();
    void testEqualityOperator2();
    void testEqualityOperator3();
    void testEqualityOperator4();
};

roomClassTest::roomClassTest()
{

}

roomClassTest::~roomClassTest()
{

}

void roomClassTest::initTestCase()
{
}

void roomClassTest::cleanupTestCase()
{

}


typedef fieldType ft;

void roomClassTest::testRoomConstruct()
{
    Room r(0,0,3,3);

    ft fields[3][3] = {
        {ft::WALL, ft::WALL, ft::WALL},
        {ft::WALL, ft::FLOOR, ft::WALL},
        {ft::WALL, ft::WALL, ft::WALL}
    };

    for(int i=0; i<3; ++i)
    {
        for(int j=0; j<3; ++j)
        {
            QCOMPARE(r.fields[i][j]->getType(),fields[i][j]);
        }
    }

}

void roomClassTest::testRoomColliding1()
{
    Room r1(0,0,10,10);
    Room r2(5,5,10,10);

    QCOMPARE(r1.isColliding(&r2), true);
}

void roomClassTest::testRoomColliding2()
{
    Room r1(0,0,5,5);
    Room r2(5,5,10,10);

    QCOMPARE(r1.isColliding(&r2), false);
}

void roomClassTest::testRoomColliding3()
{
    Room r1(0,0,5,5);
    Room r2(0,5,10,10);

    QCOMPARE(r1.isColliding(&r2), false);
}

void roomClassTest::testRoomColliding4()
{
    Room r1(0,0,6,6);
    Room r2(5,5,10,10);

    QCOMPARE(r1.isColliding(&r2), false);
}

void roomClassTest::testRoomColliding5()
{
    Room r1(0,0,4,3);
    Room r2(2,-4,5,4);

    QCOMPARE(r1.isColliding(&r2), false);
}


void roomClassTest::testRoomColliding6()
{
    Room r1(0,0,4,3);
    Room r2(0,0,4,3);

    QCOMPARE(r1.isColliding(&r2), true);
}


void roomClassTest::testRoomColliding7()
{
    Room r1(0,0,4,3);

    QCOMPARE(r1.isColliding(&r1), false);
}



void roomClassTest::testContainsField1()
{
    Room r(0,0,4,3);
    QCOMPARE(r.containsField(3, 2), true);
}


void roomClassTest::testContainsField2()
{
    Room r(0,0,4,3);
    QCOMPARE(r.containsField(-1, 2), false);
}

void roomClassTest::testContainsField3()
{
    Room r(0,0,4,3);
    QCOMPARE(r.containsField(4, 3), false);
}


void roomClassTest::testEqualityOperator1()
{
    Room r1(0,0,4,3);
    Room r2(0,0,4,3);

    QCOMPARE(r1==r2, true);
}

void roomClassTest::testEqualityOperator2()
{
    Room r1(0,0,4,3);
    Room r2(2,0,4,3);

    QCOMPARE(r1==r2, false);
}

void roomClassTest::testEqualityOperator3()
{
    Room r1(0,0,4,3);
    Room r2(0,0,5,2);

    QCOMPARE(r1==r2, false);
}

void roomClassTest::testEqualityOperator4()
{

    Room r1(0,0,4,3);

    QCOMPARE(r1==r1, true);
}


QTEST_MAIN(roomClassTest)

#include "tst_roomClassTest.moc"
