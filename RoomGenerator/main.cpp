#include <iostream>

using namespace std;


class dungeonMap{
public:
  const int xSize, ySize;
  char ** map;
  const char mapTexture, wallTexture;
public:
  dungeonMap(int xSize, int ySize, char mapTexture, char wallTexture);
  void consoleWrite();
};


dungeonMap::dungeonMap(int xSize, int ySize, char mapTexture, char wallTexture) :
  xSize(xSize), ySize(ySize), mapTexture(mapTexture),
  wallTexture(wallTexture){
  map = new char*[xSize];
  for (int i = 0; i < xSize; i++)
    map[i] = new char[ySize];

  for (int i = 0; i < xSize; i++){
    for (int j = 0; j < ySize; j++){
      if (!i || !j || i == xSize-1 || j == ySize-1)
        map[i][j] = wallTexture;
      else
        map[i][j] = mapTexture;
    }
  }
}

void dungeonMap::consoleWrite(){
  for (int i = 0; i < xSize; i++){
    for (int j = 0; j < ySize; j++){
      cout << map[i][j];
    }
    cout << "\n";
  }
}

class room{
  int x, y, width, height;
  dungeonMap *dung;
  char roomTexture, wallTexture;
public:
  room(int x, int y, int width, int height, dungeonMap *dung,
    char roomTexture, char wallTexture);
  bool generateRoom(room *r); //zwraca true jesli mozna wygenerowac, false jesli nie mozna
  int getX() { return x; }
  int getY() { return y; }
  int getWidth() { return width; }
  int getHeight() { return height; }
};

room::room(int x, int y, int width, int height, dungeonMap *dung,
  char roomTexture, char wallTexture) :
  x(x), y(y), width(width), height(height), dung(dung),
  roomTexture(roomTexture), wallTexture(wallTexture)
{
  if (width <= 0)
    throw *new string("Width value must be greater than 0!");

  if (height <= 0)
    throw *new string("Height value must be greater than 0!");

}

bool room::generateRoom(room *r){
  if (x < 0 || y < 0 || x + width > dung->xSize ||
    y + height > dung->ySize)
    return false;
  if ( r != nullptr &&
    ((x >= r->x && x <= r->x + width || x + width >= r->x && x + width <= r->x))
    && ((y >= r->y && y <= r->y + height || y + height >=
      r->y && y + height <= r->y + height)))
  {
      return false;
  }
  // cout << "Hello World!\n";
  for (int i = x; i < x + width; i++){
    for (int j = y; j < y + height; j++){
      if (i == x || i == x + width-1 || j == y || y == y + width-1)
        dung->map[i][j] = wallTexture;
      dung->map[i][j] = roomTexture;
    }
  }

  return true;
}






int main(){
  dungeonMap *mapa = new dungeonMap(20,20, ' ', '#');


  room r1(1,2,5,7,mapa,'.','$');
  r1.generateRoom(nullptr);

  mapa->consoleWrite();

}
